#!/usr/bin/python3

import os
import sys
from Caesar import Caesar

# Encrypt
print(Caesar.Caesar("Cesar Encryption", 3))

# Decrypt
print(Caesar.Caesar("FHVDU HQFUBSWLRQ", -3), "\n")

# Crack
Caesar.Crack("FHVDU HQFUBSWLRQ")

if sys.platform.startswith('win32'):
	os.system("pause")