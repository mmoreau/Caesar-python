<h1>Caesar & Rot N</h1>

<h3>What is Caesar encryption ?</h3>
<p>https://en.wikipedia.org/wiki/Caesar_cipher</p>

<h3>What is Rot N encryption ?</h3>
<p>https://en.wikipedia.org/wiki/ROT13</p>

<h3>Warnings</h3>
<p>Caution Caesar and Rot N encryption is obsolete, it is used for educational purposes to understand encryption by substitution.</p>

<h2>Terminal</h2>
<h3><b>Windows</b><h3>

<p>Encrypt a message in the terminal</p>
<pre>
    <code>
        Caesar.py -s "Hello world " -k 3 
    </code>
</pre>

<p>Decrypt a message in the terminal</p>
<pre>
    <code>
        Caesar.py -s "KHOOR ZRUOG " -k -3 
    </code>
</pre>

<p>Crack the encrypted message (option -s) to find the key to decrypt it</p>
<pre>
    <code>
        Caesar.py -s "KHOOR ZRUOG " -c
    </code>
</pre>

<p>Encrypt the contents of a ".txt" file and stores it in another file.</p>
<pre>
    <code>
        Caesar.py -f "mydata.txt" -k 3 
    </code>
</pre>

<p>Decrypt the contents of a ".txt" file and stores it in another file.</p>
<pre>
    <code>
        Caesar.py -f "mydata.txt" -k -3 
    </code>
</pre>

<h3><b>Linux</b><h3>
<p>Same command as Windows except that you have to change the script execution rights and put before ./Caesar.py<p>


<h3><b>Mac</b><h3>
<p>Same command as Windows except that you have to change the script execution rights and put before "python3 Caesar.py"<p>


<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Mail :</b> mx.moreau1@gmail.com<p>
<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>